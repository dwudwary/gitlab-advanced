# Objective #

Use metadata from the build environment to add labels to images to document their properties

Pass (some of) that metadata to the runtime environment too, so the user can make use of it

## Exercise ##

**Dockerfile.metadata** shows how to pass the metadata around. You could build the Docker image by copying **04-gitlab-ci.yml** to **.gitlab-ci.yml**, commiting and pushing the files, but we'll short-circuit that in the interest of time. Build an image by hand like this:

```
> docker build -t gitlab-advanced:latest \
         --build-arg COMMIT_ID=c2503f4389ae9ad9c617c902691baa9e1b52a913 \
         --build-arg TAG=v1.0.0 \
         --build-arg BRANCH=Feature-branch \
         -f Dockerfile.metadata .
```
Note how the **04-gitlab-ci.yml** file uses the **$CI_XYZ...** environment variables set by gitlab to set those values, you don't have to work them out by hand.

Once the image is built, inspect it to pull out the labels:
```
> docker inspect gitlab-advanced:latest | grep --after-context 10 Labels
            "Labels": {
                "gov.doe.jgi.APP_TYPE": "assembler",
                "gov.doe.jgi.BASE_IMAGE": "alpine:3.5",
                "gov.doe.jgi.BRANCH": "Feature-branch",
                "gov.doe.jgi.COMMIT_ID": "c2503f4389ae9ad9c617c902691baa9e1b52a913",
                "gov.doe.jgi.EMAIL": "wildish@lbl.gov",
                "gov.doe.jgi.INPUT_DIR": "/input",
                "gov.doe.jgi.MAINTAINER": "Tony Wildish wildish@lbl.gov",
                "gov.doe.jgi.OUTPUT_DIR": "/output",
                "gov.doe.jgi.TAG": "v1.0.0"
            }
[...]
```

See the way labels are defined, using the reverse-DNS name of the organization that built the image. This helps avoid clashes with anyone else in the world who wants to put an 'EMAIL' label in their image. E.g. if your base image had any labels they would still be there in your image, which is good for tracking provenance.

You can run the image too and see those values that were passed into the containers runtime environment:
```
> docker run gitlab-advanced:latest
Tag is v1.0.0
Branch is Feature-branch
Commit-ID is c2503f4389ae9ad9c617c902691baa9e1b52a913
```

## Best Practices ##
- see http://docs.master.dockerproject.org/v1.5/userguide/labels-custom-metadata/
- and https://speakerdeck.com/garethr/shipping-manifests-bill-of-lading-and-docker-metadata-and-container
- ...and talk with your colleagues about how to make good use of this feature!